package vue;

import java.time.LocalDate;

import com.sun.glass.ui.Application;

import application.ComiteEntreprise;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.stage.Stage;
import modele.Personne;
import modele.Salarie;
import outils.OutilsDate;

public class VueModifEnfantController {
	ComiteEntreprise mainAppli;
	@FXML
	TextField fieldNom;
	@FXML
	TextField fieldPrenom;
	@FXML
	TextField fieldAgence;
	@FXML
	DatePicker fieldDateNaissance;

	@FXML
	Button buttonAdd;
	@FXML 
	RadioButton buttonMasculin;
	@FXML 
	RadioButton buttonFeminin;	
	private Stage modifStage;
	private Personne selectedEnfant;

	@FXML
	private void initialize() {

	}

	public void setMain(ComiteEntreprise mainAppli) {
		selectedEnfant=mainAppli.getRepertoireSalarie().getSalarieSelected().getEnfantSelected();
		
		this.mainAppli = mainAppli;
		if (selectedEnfant == null) {
			Personne enfantVide = new Personne("", mainAppli.getRepertoireSalarie().getSalarieSelected().getNom(), "", OutilsDate.StringTodate("01/01/2000"));
			selectedEnfant = enfantVide;
			mainAppli.getRepertoireSalarie().getSalarieSelected().addEnfant(selectedEnfant);
		} 
		
		if (selectedEnfant.getSexe() == "Gar�on") {
			
			buttonMasculin.setSelected(true);
			
		}else {
			buttonFeminin.setSelected(true);
		}
		fieldNom.setText(selectedEnfant.getNom());
		fieldNom.setEditable(false) ;
		fieldPrenom.setText(selectedEnfant.getPrenom());
		fieldDateNaissance.setValue(OutilsDate.StringTodate(selectedEnfant.getDatenaissance()));
	}

	public void updateContact() {
		mainAppli.setSaved(false);
		LocalDate dateNaissanceParent = OutilsDate.StringTodate(mainAppli.getRepertoireSalarie().getSalarieSelected().getDatenaissance());
		LocalDate dateNaissanceEnfant = fieldDateNaissance.getValue();
		
		if(fieldNom.getText().isEmpty() ||fieldPrenom.getText().isEmpty() || fieldDateNaissance.getValue() == null  ) {

			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Veuillez entrer l'ensemble des informations du contact");
			alert.setContentText("Veuillez entrer l'ensemble des informations du contact");
			alert.show();

			
		}else if(OutilsDate.getPeriod(dateNaissanceParent, dateNaissanceEnfant).getYears() < 16){
			
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Date Naissance enfant incorrect !");
			alert.setContentText("Un enfant doit avoir 16 ans de moins que son parent");
			alert.show();
			
		}else {
		String sexe = (buttonMasculin.isSelected())? "Masculin":"Feminin";
		selectedEnfant.setAll(sexe,fieldNom.getText().toUpperCase(), fieldPrenom.getText(), fieldDateNaissance.getValue());
		modifStage.close();
		}
	}

	public void close() {
		mainAppli.setSaved(true);
		modifStage.close();
	}

	public void setStage(Stage modifStage) {
		this.modifStage = modifStage;

	}

}
