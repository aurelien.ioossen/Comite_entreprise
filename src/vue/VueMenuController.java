package vue;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import application.ComiteEntreprise;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import service.SalarieBean;

public class VueMenuController {

	private ComiteEntreprise mainAppli;
	@FXML
	private Menu menuFichiersRecent;
	@FXML
	private Label labelBottom;


	public void setMain(ComiteEntreprise mainAppli) {
		this.mainAppli = mainAppli;


	}

	
	public void nouveauFichier() throws IOException {
		FileChooser filechooser = new FileChooser();
		File selectedFileChooser = filechooser.showSaveDialog(mainAppli.getStage());
		if (selectedFileChooser != null) {
			mainAppli.getPrimaryStage().setTitle(selectedFileChooser.getPath());
			mainAppli.setRepertoireSalarie(new SalarieBean(selectedFileChooser));
			mainAppli.showContact();
		}	
		
	}
	
	
	
	
	public void ouvrirFichier() throws IOException {
		FileChooser filechooser = new FileChooser();

		File selectedFileChooser = filechooser.showOpenDialog(mainAppli.getStage());
		if (selectedFileChooser != null) {

			mainAppli.getPrimaryStage().setTitle(selectedFileChooser.getPath());
			mainAppli.setRepertoireSalarie(new SalarieBean(selectedFileChooser));
			mainAppli.showContact();
		}

	}


	public void sauvegarder() throws IOException {
		mainAppli.sauvegarder();
	}

	public void apropos() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setContentText("Cr�e par Aur�lien IOOSSEN\n 07/06/2019");
		alert.setTitle("A propos de");
		alert.showAndWait();

	}

}
