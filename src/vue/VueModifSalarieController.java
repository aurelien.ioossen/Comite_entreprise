package vue;

import java.time.LocalDate;

import com.sun.glass.ui.Application;

import application.ComiteEntreprise;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.stage.Stage;
import modele.Agence;
import modele.Personne;
import modele.Salarie;
import outils.OutilsDate;

public class VueModifSalarieController {
	ComiteEntreprise mainAppli;
	@FXML
	TextField fieldNom;
	@FXML
	TextField fieldPrenom;
	@FXML
	ComboBox<Agence> fieldAgence;
	@FXML
	DatePicker fieldDateNaissance;
	@FXML
	DatePicker fieldDateEmbauche;
	@FXML
	Button buttonAdd;
	@FXML 
	RadioButton buttonMasculin;
	@FXML 
	RadioButton buttonFeminin;	
	
	
	private Salarie selectedSalarie;
	private Stage modifStage;

	@FXML
	private void initialize() {

	}

	public void setMain(ComiteEntreprise mainAppli) {

		this.mainAppli = mainAppli;
		if (mainAppli.getRepertoireSalarie().getSalarieSelected() == null) {
			Salarie salarieVide = new Salarie("", "", "", "", OutilsDate.StringTodate("01/01/2010"), OutilsDate.StringTodate("01/01/2000"));
			selectedSalarie = salarieVide;
		} else {
			this.selectedSalarie = mainAppli.getRepertoireSalarie().getSalarieSelected();
		}
		
		if (selectedSalarie.getSexe() == "Masculin") {
			
			buttonMasculin.setSelected(true);
			
		}else {
			buttonFeminin.setSelected(true);
		}
		fieldNom.setText(selectedSalarie.getNom());
		fieldPrenom.setText(selectedSalarie.getPrenom());
		fieldAgence.setItems(mainAppli.getListeAgence());
		fieldAgence.setValue(selectedSalarie.getAgence());
		fieldDateNaissance.setValue(OutilsDate.StringTodate(selectedSalarie.getDatenaissance()));
		fieldDateEmbauche.setValue(OutilsDate.StringTodate(selectedSalarie.getDateEmbauche()));
	}

	public void updateContact() {
		mainAppli.setSaved(false);
		if(fieldNom.getText().isEmpty() ||fieldPrenom.getText().isEmpty() || fieldDateNaissance.getValue() == null || fieldDateEmbauche.getValue() == null ) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Veuillez entrer l'ensemble des informations du contact");
			alert.setContentText("Veuillez entrer l'ensemble des informations du contact");
			alert.show();

			
		}else if (OutilsDate.getPeriod(fieldDateNaissance.getValue(), LocalDate.now()).getYears() < 16) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Le salari� doit avoir plus de 16 ans");
			alert.setContentText("Le salari� doit avoir plus de 16 ans");
			alert.show();
			
		}else {
		String sexe = (buttonMasculin.isSelected())? "Masculin":"Feminin";
		selectedSalarie.setAll(sexe,fieldNom.getText().toUpperCase(), fieldPrenom.getText(), fieldAgence.getSelectionModel().getSelectedItem(), fieldDateNaissance.getValue(),fieldDateEmbauche.getValue());
		mainAppli.getRepertoireSalarie().ajouterSalarie((Salarie) selectedSalarie);
		modifStage.close();
		}

	}

	public void close() {
		mainAppli.setSaved(true);
		modifStage.close();
	}

	public void setStage(Stage modifStage) {
		this.modifStage = modifStage;

	}

}
