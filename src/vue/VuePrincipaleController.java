package vue;

import java.awt.print.PrinterException;
import java.util.Optional;

import application.ComiteEntreprise;
import javafx.fxml.FXML;
import javafx.print.PrinterJob;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import modele.Agence;
import modele.Personne;
import modele.Salarie;
import service.SalarieBean;

public class VuePrincipaleController {
	private ComiteEntreprise mainApplication;
	private SalarieBean repertoireSalaries;
	AnchorPane anchorPane;
	@FXML
	private Label selectedNom;
	@FXML
	private Label selectedPrenom;
	@FXML
	private Label selectedDateNaissance;
	@FXML
	private Label selectedAgence;
	@FXML
	private Label selectedDateEmbauche;
	@FXML
	private Label selectedChequeVacance;
	@FXML
	private Label selectedChequeNoel;
	@FXML
	private Label selectedChequeRestaurant;
	@FXML
	private Button buttonSuprrimer;
	@FXML
	private Button buttonModifier;
	@FXML
	private Button buttonGestionEnfant;
	@FXML
	private RadioButton buttonSexeFemme;
	@FXML
	private RadioButton buttonSexeHomme;
	@FXML
	private RadioButton buttonChequeNoel;
	@FXML
	private RadioButton buttonChequeVacance;	
	@FXML
	private TextField fieldRecherche;
	@FXML
	TableView<Salarie> tableSalarie;
	@FXML
	private TableColumn<Salarie, String> colonneNom;
	@FXML
	private TableColumn<Salarie, String> colonnePrenom;
	@FXML
	private TableColumn<Salarie, String> colonneDateNaissance;
	@FXML
	private TableColumn<Salarie, String> colonneDateEmbauche;
	@FXML
	private TableColumn<Salarie, Agence> colonneAgence;
	@FXML
	private ComboBox<Agence> cbAgence;
	@FXML
	private ToggleGroup toggleSexeSearched;

	public void setRoot(AnchorPane root) {
		anchorPane = root;
	}

	@FXML
	private void initialize() {

		// remplissage tableau

		colonneNom.setCellValueFactory((CellDataFeatures<Salarie, String> feature) -> feature.getValue().nomProperty());

		colonnePrenom.setCellValueFactory(
				(CellDataFeatures<Salarie, String> feature) -> feature.getValue().prenomProperty());

		colonneDateNaissance.setCellValueFactory(
				(CellDataFeatures<Salarie, String> feature) -> feature.getValue().datenaissanceProperty());

		colonneAgence.setCellValueFactory(
				(CellDataFeatures<Salarie, Agence> feature) -> feature.getValue().agenceProperty());

		// ajout listener sur la personne selectionnée
		tableSalarie.getSelectionModel().selectedItemProperty()
				.addListener((observable, oldValue, newValue) -> afficherDetail(newValue));

	}

	private void afficherDetail(Personne newValue) {
		repertoireSalaries.setSalarieSelected((Salarie) newValue);
		selectedNom.setText(newValue.getNom());
		selectedPrenom.setText(newValue.getPrenom());
		selectedDateNaissance.setText(newValue.getDatenaissance());
		selectedAgence.setText(((Salarie) newValue).getAgence().getNom());
		selectedDateEmbauche.setText(((Salarie) newValue).getDateEmbauche());
		selectedChequeRestaurant.setText(((Salarie) newValue).getDroit().isTicketRestaurant());
		selectedChequeVacance.setText(((Salarie) newValue).getDroit().isChequesVacances());
		selectedChequeNoel.setText(((Salarie) newValue).getDroit().listerDroitEnfant() + "€");

		buttonModifier.setDisable(false);
		buttonSuprrimer.setDisable(false);
		buttonGestionEnfant.setDisable(false);

		System.out.println(((Salarie) newValue).getListeEnfant());

	}

	public void setMain(ComiteEntreprise mainApplication) {
		this.mainApplication = mainApplication;
		repertoireSalaries = mainApplication.getRepertoireSalarie();
		tableSalarie.setItems(repertoireSalaries.getSortedList());
		repertoireSalaries.getSortedList().comparatorProperty().bind(tableSalarie.comparatorProperty());
		cbAgence.setItems(mainApplication.getListeAgence());

		buttonModifier.setDisable(true);
		buttonSuprrimer.setDisable(true);
		buttonGestionEnfant.setDisable(true);

	}

	public void supprimerContact() {
		Alert fenetreAlerte = new Alert(AlertType.WARNING);
		fenetreAlerte.setTitle("Confirmation de suppression");
		fenetreAlerte.setContentText(
				"Confirmez vous la suppression de : " + repertoireSalaries.getSalarieSelected().toString());
		Optional<ButtonType> resultat = fenetreAlerte.showAndWait();
		if (resultat.isPresent() && resultat.get() == ButtonType.OK) {
			repertoireSalaries.supprimerSalarie(repertoireSalaries.getSalarieSelected());
			tableSalarie.getSelectionModel().select(null);
			mainApplication.setSaved(true);

		}
	}

	public void modifierContact() {

		mainApplication.showModificationSalarie();

	}

	public void ajouterContact() {
		tableSalarie.getSelectionModel().select(null);
		mainApplication.showModificationSalarie();
		mainApplication.setSaved(true);

	}

	public void filtrerContact() {
		
		mainApplication.getRepertoireSalarie().filterContact(fieldRecherche.getText(),getSexeSearched(),cbAgence.getSelectionModel().getSelectedItem(),isChequeVacanceSearched(),isChequeNoelsearched());

	}

	private Boolean isChequeNoelsearched() {
		if (buttonChequeNoel.isSelected()) {
			return true;

		}else return false;
	}

	private Boolean isChequeVacanceSearched() {
		if (buttonChequeVacance.isSelected()) {
			return true;

		}else return false;
	}

	public void gererEnfant() {
		mainApplication.showGestionEnfant();

	}

	public void afficherDroit() {
		mainApplication.showDroitSalarie(repertoireSalaries.getSalarieSelected());

	}

	public void imprimer() throws PrinterException {

		PrinterJob printerJob = PrinterJob.createPrinterJob();
		printerJob.showPrintDialog(mainApplication.getStage());
		boolean success = printerJob.printPage(tableSalarie);
		if (success) {
			printerJob.endJob();
		} else
			printerJob.cancelJob();

	}

	public Agence getCbAgence() {
		return cbAgence.getValue();
	}

	public void setCbAgence(ComboBox<Agence> cbAgence) {
		this.cbAgence = cbAgence;
	}

	public boolean isNomRecherche() {
		if (fieldRecherche.getText() != null) {
			return true;
		}else return false;
		
	}
	
	public TextField getFieldRecherche() {
		return fieldRecherche;
	}

	public void setFieldRecherche(TextField fieldRecherche) {
		this.fieldRecherche = fieldRecherche;
	}

	public String getSexeSearched() {
		if (buttonSexeHomme.isSelected()) {
			return "Masculin";

		} else if(buttonSexeFemme.isSelected()) {
			
		
			return "Feminin";
		}else return "Tous";
		}
	
	
	public boolean isSexeSearched() {
		if (toggleSexeSearched.getSelectedToggle() != null) {
			
			return true;
		}else return false;
	}

	public boolean isAgenceSearched() {
		if (cbAgence.getValue() != null) {		
			return true;
		}else return false;
		
	}
	
	
}
