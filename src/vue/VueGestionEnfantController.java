package vue;

import java.awt.print.PrinterException;
import java.util.ArrayList;
import java.util.Optional;

import application.ComiteEntreprise;
import javafx.beans.property.Property;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.print.PrinterJob;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import modele.Personne;
import modele.Salarie;
import service.SalarieBean;

public class VueGestionEnfantController {
	private ComiteEntreprise mainApplication;
	private ObservableList<Personne> listeEnfant;
	AnchorPane anchorPane;
	@FXML
	private Label nomParent;
	@FXML
	private Label selectedNom;
	@FXML
	private Label selectedPrenom;
	@FXML
	private Label selectedDateNaissance;
	@FXML
	private Label selectedDateEmbauche;
	@FXML
	private Button buttonSuprrimer;
	@FXML
	private Button buttonModifier;
	@FXML
	private TextField fieldRecherche;
	@FXML
	private javafx.scene.control.TableView<Personne> tableEnfants;
	@FXML
	private TableColumn<Personne, String> colonneSexe;
	@FXML
	private TableColumn<Personne, String> colonnePrenom;
	@FXML
	private TableColumn<Personne, String> colonneDateNaissance;
	private SalarieBean repertoireSalarie;

	public void setRoot(AnchorPane root) {
		anchorPane = root;
	}

	@FXML
	private void initialize() {

		// remplissage tableau

		colonneSexe
				.setCellValueFactory((CellDataFeatures<Personne, String> feature) -> feature.getValue().sexeProperty());

		colonnePrenom.setCellValueFactory(
				(CellDataFeatures<Personne, String> feature) -> feature.getValue().prenomProperty());

		colonneDateNaissance.setCellValueFactory(
				(CellDataFeatures<Personne, String> feature) -> feature.getValue().datenaissanceProperty());

		// ajout listener sur la personne selectionnée
		tableEnfants.getSelectionModel().selectedItemProperty()
				.addListener((observable, oldValue, newValue) -> afficherDetail(newValue));

	}

	private void afficherDetail(Personne newValue) {
		repertoireSalarie.getSalarieSelected().setEnfantSelected(newValue);
		
		System.out.println("Enfant selectionné :"+repertoireSalarie.getSalarieSelected().getEnfantSelected());

		// selectedNom.setText(newValue.getNom());
//		selectedPrenom.setText(newValue.getPrenom());
//		selectedDateNaissance.setText(newValue.getDatenaissance());
//		selectedAgence.setText(((Salarie) newValue).getAgence());
//		selectedDateEmbauche.setText(((Salarie) newValue).getDateEmbauche());

		buttonModifier.setDisable(false);
		buttonSuprrimer.setDisable(false);

	}

	public void setMain(ComiteEntreprise mainApplication) {
		this.mainApplication = mainApplication;
		repertoireSalarie = mainApplication.getRepertoireSalarie();
		

		listeEnfant = repertoireSalarie.getSalarieSelected().getListeEnfant();
		
		tableEnfants.setItems(listeEnfant);
		buttonModifier.setDisable(true);
		buttonSuprrimer.setDisable(true);
		nomParent.setText(mainApplication.getRepertoireSalarie().getSalarieSelected().getNom() + " "
				+ mainApplication.getRepertoireSalarie().getSalarieSelected().getPrenom());

	}

	public void supprimerEnfant() {
		Alert fenetreAlerte = new Alert(AlertType.WARNING);
		fenetreAlerte.setTitle("Confirmation de suppression");
		fenetreAlerte.setContentText("Confirmez vous la suppression ?");
		Optional<ButtonType> resultat = fenetreAlerte.showAndWait();
		if (resultat.isPresent() && resultat.get() == ButtonType.OK) {
			System.out.println("Suppression enfant :"+	repertoireSalarie.getSalarieSelected().getEnfantSelected().getPrenom());
			repertoireSalarie.getSalarieSelected().removeEnfant(repertoireSalarie.getSalarieSelected().getEnfantSelected());
			System.out.println("Liste actualisée enfant :"+repertoireSalarie.getSalarieSelected().getListeEnfant());
			tableEnfants.getSelectionModel().select(null);
			buttonModifier.setDisable(true);
			buttonSuprrimer.setDisable(true);			
			mainApplication.setSaved(false);

		}

	}

	public void modifierEnfant() {

		mainApplication.showModificationEnfant();

	}

	public void ajouterEnfant() {
		tableEnfants.getSelectionModel().select(null);
		mainApplication.showModificationEnfant();
		mainApplication.setSaved(false);

	}

//	public void filtrerEnfant() {
//		mainApplication.getRepertoireSalarie().filterContact(fieldRecherche.getText());
//
//	}

	public void imprimer() throws PrinterException {

		PrinterJob printerJob = PrinterJob.createPrinterJob();
		printerJob.showPrintDialog(mainApplication.getStage());
		boolean success = printerJob.printPage(tableEnfants);
		if (success) {
			printerJob.endJob();
		} else
			printerJob.cancelJob();

	}

}
