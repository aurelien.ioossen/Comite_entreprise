package service;

import java.io.File;

import dao.SalarieDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import modele.Agence;
import modele.Personne;
import modele.Salarie;
import outils.OutilsDate;
import vue.VuePrincipaleController;

public class SalarieBean {
	private SalarieDAO dao;
	private ObservableList<Salarie> allSalarie;
	private FilteredList<Salarie> filteredSalarie;
	private SortedList<Salarie> sortedSalarie;
	private Salarie salarieSelected;
	private Salarie salarieSearched;

	public SalarieBean(File selectedFileChooser) {

		dao = new SalarieDAO(selectedFileChooser);
		allSalarie = FXCollections.observableArrayList(dao.genererSalaries());
		filteredSalarie = new FilteredList<>(allSalarie, null);
		sortedSalarie = new SortedList<>(filteredSalarie);
		

	}

	public ObservableList<Salarie> getAllSalarie() {
		return allSalarie;
	}

	public Salarie getSalarieSelected() {
		return salarieSelected;
	}

	public void setSalarieSelected(Salarie salarieSelected) {
		this.salarieSelected = salarieSelected;
	}

	public SortedList<Salarie> getSortedList() {
		return sortedSalarie;
	}

	public void filterContact(String nom, String sexe, Agence agence, Boolean isChequeVacance, Boolean isChequeNoel) {

//		filteredSalarie.setPredicate(salarie -> {
//	
//			boolean boolNom = salarie.getNom().contains(nom);
//			
//			boolean boolAgence = agence == null ? true : salarie.getAgence().equals(agence);
//			
//			boolean boolSexe = sexe.equals("Tous")? true:salarie.getSexe().equals(sexe);
			
			
			//TO DO LIST
			
//			boolean boolChequeVacances = isChequeVacance.equals(true)? salarie.getDroitSalarie().isChequesVacances():true;
//			
//			boolChequeNoel = ;
//
//			return boolNom && boolAgence && boolSexe && boolChequeVacances && boolChequeNoel;

			
			
		});
	}

	public void supprimerSalarie(Salarie salarie) {
		if (allSalarie.contains(salarie)) {

			allSalarie.remove(salarie);
		}
	}

	public void ajouterSalarie(Salarie selectedPersonne) {
		allSalarie.add(selectedPersonne);
	}

	public void sauvegarder() {
		dao.save(allSalarie);

	}

}
