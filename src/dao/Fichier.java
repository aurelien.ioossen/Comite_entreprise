package dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class Fichier {

	private File selectedFileChooser;
	private ArrayList<String> lignes = new ArrayList<>();

	public Fichier(File selectedFileChooser) {
		this.selectedFileChooser = selectedFileChooser;
	}

	public ArrayList<String> lire() {

		try (BufferedReader fichier = new BufferedReader(new FileReader(selectedFileChooser))) {

			String line;
			while ((line = fichier.readLine()) != null) {

				lignes.add(line);
				
			}
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setContentText("Erreur lors de l'ouverture du fichier !");
		}
		return lignes;

	}

	public void ecrire(ArrayList<String> lignes) {
		try (BufferedWriter bf = new BufferedWriter(new FileWriter(selectedFileChooser))) {
			for (String s : lignes) {
				System.out.println("Ecriture de la ligne :" + s + " dans le fichier " + selectedFileChooser);
				bf.append(s);
				bf.newLine();

			}

		} catch (IOException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setContentText("Erreur lors de l'ouverture du fichier !");
			e.printStackTrace();
		}

	}

}
