package dao;

import java.io.File;
import java.time.LocalDate;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.Iterator;

import javafx.collections.ObservableList;
import javafx.util.Callback;
import modele.Personne;
import modele.Salarie;
import outils.OutilsDate;

public class SalarieDAO {

	private Fichier selectedFileChooser;
	private ArrayList<Salarie> listeSalarie;
	private Salarie lastCreated;

	public SalarieDAO(File selectedFileChooser2) {
		selectedFileChooser = new Fichier(selectedFileChooser2);

	}

	public ArrayList<Salarie> genererSalaries() {
		System.out.println("G�n�ration des Salari�s :");
		listeSalarie = new ArrayList<>();

		for (String salarieString : selectedFileChooser.lire()) {
			String[] tabSalarie = salarieString.split("\\|");

			if (tabSalarie[0].equals("Salari�")) {

				String sexe = (tabSalarie[1].equals("1")) ? "Masculin" : "Feminin";
				String prenom = tabSalarie[2];
				LocalDate dateNaissance = OutilsDate.StringTodate(tabSalarie[3]);
				String nom = tabSalarie[4];
				String agence = tabSalarie[5];
				LocalDate dateEmbauche = OutilsDate.StringTodate(tabSalarie[6]);

				System.out.printf("Ajout du Salarie : %s, %s, %s, %s,n� le :%s, embauch� le :%s%n", sexe, nom, prenom,
						agence, dateNaissance, dateEmbauche);
				Salarie newSalarie = new Salarie(sexe, nom, prenom, agence, dateNaissance, dateEmbauche);
				listeSalarie.add(newSalarie);
				lastCreated = newSalarie;

			} else if (tabSalarie[0].equals("Enfant")) {

				String[] tabEnfant = salarieString.split("\\|");
				String sexe = (tabEnfant[1].equals("1")) ? "Gar�on" : "Fille";
				String prenom = tabEnfant[2];
				LocalDate dateNaissance = OutilsDate.StringTodate(tabEnfant[3]);
				String nom = lastCreated.getNom();

				System.out.printf("Ajout de l'enfant : %s, %s, %s, %s%n", sexe, nom, prenom, dateNaissance);
				lastCreated.addEnfant(new Personne(sexe, nom, prenom, dateNaissance));

			}

		}
		return listeSalarie;
	}

	public void save(ObservableList<Salarie> allSalarie) {
		System.out.println("Sauvegarde des Salari�s et de leurs enfants :");
		ArrayList<String> listeLigne = new ArrayList<>();
		listeLigne.add("Type|Sexe|Pr�nom|DateNaissance|Nom|Agence|DateEmbauche");
		for (Salarie salarie : allSalarie) {

			String type = "Salari�";
			String sexe = (salarie.getSexe() == "Masculin") ? "1" : "2";
			String prenom = salarie.getPrenom();
			String datenaissance = salarie.getDatenaissance();
			String nom = salarie.getNom();
			String agence = salarie.getAgence().getNom();
			String dateEmbauche = salarie.getDateEmbauche();
			
			listeLigne.add(type+"|"+sexe+"|"+prenom+"|"+datenaissance+"|"+nom+"|"+agence+"|"+dateEmbauche);
			if (salarie.getListeEnfant() != null) {
				for (Personne enfant : salarie.getListeEnfant()) {
					String typeEnfant="Enfant";
					String sexeEnfant = (enfant.getSexe() == "Masculin") ? "1" : "2";
					String prenomEnfant = enfant.getPrenom();
					String datenaissanceEnfant = enfant.getDatenaissance();
					
					listeLigne.add(typeEnfant+"|"+sexeEnfant+"|"+prenomEnfant+"|"+datenaissanceEnfant);
				}
			}

		}

		selectedFileChooser.ecrire(listeLigne);
	}

}
