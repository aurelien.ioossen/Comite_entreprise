package application;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.stage.Modality;
import javafx.stage.Stage;
import modele.Agence;
import modele.Salarie;
import service.SalarieBean;
import vue.VueGestionEnfantController;
import vue.VueMenuController;
import vue.VueModifEnfantController;
import vue.VueModifSalarieController;
import vue.VuePrincipaleController;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

public class ComiteEntreprise extends Application {

	private Stage primaryStage;
	private BorderPane root;
	private boolean isSaved = true;
	private SalarieBean repertoireSalaries;
	private ObservableList<Agence> listeAgence;

	@Override
	public void start(Stage primaryStage) {
		isSaved = true;
		listeAgence = FXCollections.observableArrayList(new ArrayList<>());
		genererAgence();
		this.setPrimaryStage(primaryStage);

		primaryStage.setTitle("Gestion comit� entreprise");
//		primaryStage.setFullScreen(true);
		primaryStage.setOnCloseRequest(event -> {
			sauvegarder();
		});

		showMenu();

	}

	private void genererAgence() {
		listeAgence.add(null);
		listeAgence.add(new Agence("Roubaix"));
		listeAgence.add(new Agence("Dunkerque"));
		listeAgence.add(new Agence("Lille"));

	}

	public BorderPane getRoot() {
		return root;
	}

	public SalarieBean getRepertoireSalarie() {
		return repertoireSalaries;
	}

	public void setRepertoireSalarie(SalarieBean salarieBean) {
		repertoireSalaries = salarieBean;
	}

	public void sauvegarder() {
		if (!isSaved) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Sauvegarder ?");
			alert.setContentText("Des modifications ont �t� effectu�es !\nVoulez vous sauvegarder ?");
			Optional<ButtonType> resultat = alert.showAndWait();
			if (resultat.isPresent() && resultat.get() == ButtonType.OK) {
				repertoireSalaries.sauvegarder();
			}

		}

	}

	private void showMenu() {
		try {
			FXMLLoader myFXMLloader = new FXMLLoader();
			myFXMLloader.setLocation(ComiteEntreprise.class.getResource("../vue/VueMenu.fxml"));
			root = myFXMLloader.load();

			// D�claration et instanciation de la sc�ne contenant le layout
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			// primaryStage.setFullScreen(true);
			getPrimaryStage().setScene(scene);
			// D�marrage du Stage
			getPrimaryStage().show();
			// D�claration et instanciation du controller
			VueMenuController MenuController = myFXMLloader.getController();
			MenuController.setMain(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}

	public Stage getStage() {
		return getPrimaryStage();
	}

	public void showContact() throws IOException {
		FXMLLoader myFXMLloader = new FXMLLoader();
		myFXMLloader.setLocation(ComiteEntreprise.class.getResource("../vue/VuePrincipale.fxml"));
		AnchorPane layoutContact = new AnchorPane();
		layoutContact = myFXMLloader.load();
		layoutContact.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		root.setCenter(layoutContact);

		// D�claration et instanciation du controller
		VuePrincipaleController controller = myFXMLloader.getController();
		controller.setMain(this);

	}

	public void showModificationSalarie() {

		AnchorPane rootModif = new AnchorPane();
		Stage stageModif = new Stage();
		stageModif.setTitle("Modification Salari�");
		FXMLLoader myFXMLloader2 = new FXMLLoader();
		myFXMLloader2.setLocation(ComiteEntreprise.class.getResource("../vue/VueModifSalarie.fxml"));
		try {
			rootModif = myFXMLloader2.load();
			stageModif.initModality(Modality.WINDOW_MODAL);
			stageModif.initOwner(getPrimaryStage());
			Scene sceneModif = new Scene(rootModif);
			stageModif.setScene(sceneModif);
			VueModifSalarieController contactsModifController = myFXMLloader2.getController();
			contactsModifController.setMain(this);
			contactsModifController.setStage(stageModif);
			stageModif.showAndWait();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public boolean isSaved() {
		return isSaved;
	}

	public void setSaved(boolean isSaved) {
		this.isSaved = this.isSaved && isSaved;
	}

	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}

	public void showGestionEnfant() {
		AnchorPane rootModif = new AnchorPane();
		Stage stageModif = new Stage();
		stageModif.setTitle("Gestion des Enfants du Salari� : " + repertoireSalaries.getSalarieSelected().getNom() + " "
				+ repertoireSalaries.getSalarieSelected().getPrenom());
		FXMLLoader myFXMLloader2 = new FXMLLoader();
		myFXMLloader2.setLocation(ComiteEntreprise.class.getResource("../vue/VueGestionEnfant.fxml"));
		try {
			rootModif = myFXMLloader2.load();
			stageModif.initModality(Modality.WINDOW_MODAL);
			stageModif.initOwner(getPrimaryStage());
			Scene sceneModif = new Scene(rootModif);
			stageModif.setScene(sceneModif);
			VueGestionEnfantController gestionEnfantController = myFXMLloader2.getController();
			gestionEnfantController.setMain(this);
			stageModif.showAndWait();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void showDroitSalarie(Salarie salarieSelected) {
		// TODO Auto-generated method stub

	}

	public void showModificationEnfant() {
		AnchorPane rootModif = new AnchorPane();
		Stage stageModif = new Stage();
		stageModif.setTitle("Modification d'un Enfant");
		FXMLLoader myFXMLloader2 = new FXMLLoader();
		myFXMLloader2.setLocation(ComiteEntreprise.class.getResource("../vue/VueModifEnfant.fxml"));
		try {
			rootModif = myFXMLloader2.load();
			stageModif.initModality(Modality.WINDOW_MODAL);
			stageModif.initOwner(getPrimaryStage());
			Scene sceneModif = new Scene(rootModif);
			stageModif.setScene(sceneModif);
			VueModifEnfantController enfantModifController = myFXMLloader2.getController();
			enfantModifController.setMain(this);
			enfantModifController.setStage(stageModif);
			stageModif.showAndWait();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ObservableList<Agence> getListeAgence() {
		return listeAgence;
	}

}
