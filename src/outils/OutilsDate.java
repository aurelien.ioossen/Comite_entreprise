package outils;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class OutilsDate {

	private static DateTimeFormatter formatNum = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	private static DateTimeFormatter formatChaine = DateTimeFormatter.ofPattern("d MMMM yyyy");

	public static LocalDate StringTodate(String date) {
		return LocalDate.parse(date, formatNum);
	}

	public static String dateToString(LocalDate date) {

		return formatNum.format(date);

	}

	public static Period getPeriod(LocalDate dateDebut, LocalDate dateFin) {

		Period p = Period.between(dateDebut, dateFin);
		// System.out.println(p);
		return p;

	}
}
