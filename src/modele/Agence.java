package modele;

import java.io.File;
import java.util.ArrayList;

import dao.SalarieDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;

public class Agence {
	private String nom;
	private ArrayList<Salarie> listSalaries;
	private boolean restaurantEntreprise;



	public Agence(String nom) {
		super();
		this.nom = nom;
		if (nom.equals("Dunkerque")) {
			restaurantEntreprise =false;
		}else {
			restaurantEntreprise =true;
		}
		
		listSalaries= new ArrayList<>();

	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public ArrayList<Salarie> getListSalaries() {
		return listSalaries;
	}

	public void setListSalaries(ArrayList<Salarie> listSalaries) {
		this.listSalaries = listSalaries;
	}
	
	
	public void ajouterSalarie(Salarie salarie) {
		listSalaries.add(salarie);
		
	}
	
	public void supprimerSalarie(Salarie salarie) {
		listSalaries.remove(salarie);
	}

	public boolean isRestaurantEntreprise() {
		return restaurantEntreprise;
	}

	public void setRestaurantEntreprise(boolean restaurantEntreprise) {
		this.restaurantEntreprise = restaurantEntreprise;
	}

	@Override
	public String toString() {
		return nom;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		boolean egal=false;
		if (obj instanceof Agence) {
			
			if (((Agence) obj).getNom().equals(nom)) {
				egal= true;
			}else egal= false;
	
}
		return egal ;
	}


	
	
	
	

}
