package modele;

import java.time.LocalDate;
import javafx.beans.InvalidationListener;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleSetProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import outils.OutilsDate;

public class Personne {

	private StringProperty sexe;
	private StringProperty prenom;
	private StringProperty nom;
	private StringProperty dateNaissance;

	public Personne(String sexe, String nom, String prenom, LocalDate dateNaissance) {
		this.sexe = new SimpleStringProperty();
		this.prenom =  new SimpleStringProperty();
		this.nom = new SimpleStringProperty();
		this.dateNaissance = new SimpleStringProperty();
		setAll(sexe, nom, prenom, dateNaissance);
	}

	public void setAll(String sexe2, String nom2, String prenom2, LocalDate dateNaissance2) {
		this.sexe.set(sexe2);
		this.nom.set(nom2);	
		this.prenom.set(prenom2);	
		this.dateNaissance.set(OutilsDate.dateToString(dateNaissance2));
		
	}

	public String getNom() {
		return nom.get();
	}

	public void setNom(StringProperty nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom.get();
	}

	public void setPrenom(StringProperty prenom) {
		this.prenom = prenom;
	}

	public String getDatenaissance() {
		return dateNaissance.get();
	}
	
	public String getSexe() {
		return sexe.get();
	}

	public void setDatenaissance(StringProperty datenaissance) {
		this.dateNaissance = datenaissance;
	}

	@Override
	public String toString() {
		return "Personne [nom=" + nom + ", prenom=" + prenom + ", datenaissance=" + dateNaissance + "]";
	}

	public final StringProperty nomProperty() {
		return this.nom;
	}
	
	public final StringProperty prenomProperty() {
		return this.prenom;
	}
	
	public final StringProperty datenaissanceProperty() {
		return this.dateNaissance;
	}

	public final StringProperty sexeProperty() {
		return this.sexe;
	}

	public int getAge() {
		LocalDate dateCalcul = OutilsDate.StringTodate("31/12/"+LocalDate.now().getYear());
		int nmbreAnee = OutilsDate.getPeriod(OutilsDate.StringTodate(dateNaissance.getValue()), dateCalcul).getYears() ;
		System.out.println("Age = "+nmbreAnee);

		return nmbreAnee;
		
		
	}
	

	
	
	
	

}
