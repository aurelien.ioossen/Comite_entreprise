package modele;

import java.util.ArrayList;
import java.util.List;

public class Droit {

	Salarie leSalarie;

	public Droit(Salarie salarie) {
		this.leSalarie = salarie;
	}

	public String isTicketRestaurant() {

		if (leSalarie.getAgence().isRestaurantEntreprise()) {
			System.out.println("Le salari� � le droit au ticket restaurant");
			return "Oui";
		} else return "Non";
	}

	public String isChequesVacances() {

		int mois = leSalarie.getAnciennete().getYears() * 12 + leSalarie.getAnciennete().getMonths();

		if (mois > 9) {
			System.out.println("Le salari� a des cheques vacances :" + mois + " mois d'anciennet�");
			return "Oui" ;
		}else return "Non";
	}

	public <T> String listerDroitEnfant() {
		List<Personne> enfantSalarie = leSalarie.getListeEnfant() ;
		int chequeNoel = 0;
		System.out.println("DROITS CHEQUE NOEL :");
		for (int i = 0; i < enfantSalarie.size(); i++) {
			Personne enfant = (Personne) enfantSalarie.get(i);
			System.out.println("Pour l'enfant" + enfant);
			if ((enfant.getAge()) >= 16 && enfant.getAge() <= 18) {
				System.out.println("50�");
				chequeNoel+=50;

			} else if ((enfant.getAge()) >= 11 && enfant.getAge() <= 15) {
				System.out.println("30�");
				chequeNoel+=30;

			} else if ((enfant.getAge()) >= 0 && enfant.getAge() <= 10) {
				System.out.println("20�");
				chequeNoel+=20;

			}

		}
		return String.valueOf(chequeNoel);

	}

}
