package modele;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;

import javafx.beans.InvalidationListener;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import outils.OutilsDate;

public class Salarie extends Personne {

	private LocalDate dateEmbaucheDate;
	private SimpleObjectProperty<Agence> agence;
	private ObservableList<Personne> listeEnfant;
	private Personne enfantSelected;
	private Droit droitSalarie;

	public Salarie(String sexe, String nom, String prenom, String agence, LocalDate dateNaissance,
			LocalDate dateEmbaucheDate) {
		super(sexe, nom, prenom, dateNaissance);
		this.dateEmbaucheDate = dateEmbaucheDate;
		this.agence = new SimpleObjectProperty<Agence>(new Agence(agence));
		listeEnfant = FXCollections.observableArrayList(new ArrayList<Personne>());
		droitSalarie = new Droit(this);
	
	}

	public void setAll(String sexe, String nom, String prenom, Agence agence, LocalDate datenaissance,
			LocalDate dateEmbauche) {
		super.setAll(sexe, nom, prenom, datenaissance);
		this.dateEmbaucheDate = dateEmbauche;
		this.agence = new SimpleObjectProperty<>(agence);

	}

	public String getDateEmbauche() {
		return OutilsDate.dateToString(dateEmbaucheDate);
	}

	public StringProperty dateEmbaucheProperty() {
		return new SimpleStringProperty(OutilsDate.dateToString(dateEmbaucheDate));

	}

	public void setDateEmbaucheDate(LocalDate dateEmbaucheDate) {
		this.dateEmbaucheDate = dateEmbaucheDate;
	}

	public void setAgence(SimpleObjectProperty<Agence> agence) {
		this.agence = agence;
	}

	public ObservableList<Personne> getListeEnfant() {
		System.out.println("Liste des Enfants : " + listeEnfant);
		return listeEnfant;
	}

	public void addEnfant(Personne enfant) {
		listeEnfant.add(enfant);
	}

	public void removeEnfant(Personne enfant) {
		listeEnfant.remove(enfant);
	}

	public Droit getDroitSalarie() {
		return droitSalarie;
	}

	public void setDroitSalarie(Droit droitSalarie) {
		this.droitSalarie = droitSalarie;
	}

	public SimpleObjectProperty<Agence> agenceProperty() {
		return agence;

	}

	public Agence getAgence() {
		return agence.get();
	}

	public Personne getEnfantSelected() {
		return enfantSelected;
	}

	public void setEnfantSelected(Personne enfantSelected) {
		this.enfantSelected = enfantSelected;
	}

	public Period getAnciennete() {
		Period p = OutilsDate.getPeriod(dateEmbaucheDate, LocalDate.now());
		return p;

	}
	
	
	public Droit getDroit() {
		return droitSalarie;
	}
	
}
